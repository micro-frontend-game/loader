Micro Front-End Loader
======================

A Loader for Micro Front-End components that extends `BaseComponent`.

You must require it only once, on the app launcher and it will pass itself for other internal components loaded by `BaseComponent`'s `loadComponent()` method.
