"use strict";

export default function loadComponent(urlOrClass, shared) {
  const argIsClass = typeof(urlOrClass) === 'function'
  const klass = argIsClass ? urlOrClass : null
  const url = argIsClass ? null : urlOrClass
  return (argIsClass ? Promise.resolve({default: klass}) : import(url))
  .then(module => {
    const wrapper = {
      Component: module.default,
      create(options) {
        const el = this.Component.__create(options)
        Object.defineProperty(el, 'shared', { get() { return shared } })
        el.loadComponent = (url, newShared)=> {
          return loadComponent(url, { ...shared, ...newShared })
        }
        return el
      }
    }
    return wrapper
  })
  .catch(err => {
    const name = argIsClass ? `Class ${klass.name}` : url
    Object.assign(err, { message: err.message +': '+ name })
    throw err
  })
}
